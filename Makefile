# Makefile

all:
	rm -rf gcm.cache *.o
	g++ -std=c++17 -fmodules-ts -x c++-system-header iostream
	g++ -std=c++17 -fmodules-ts -x c++-system-header string
	g++ -std=c++17 -fmodules-ts -c -x c++ cat.cpp -o cat.o
	g++ -std=c++17 -fmodules-ts -c -x c++ dog.cpp -o dog.o
	g++ -std=c++17 -fmodules-ts -c -x c++ animals.cpp -o animals.o
	g++ -std=c++17 -fmodules-ts main.cpp -o main.exe animals.o cat.o dog.o

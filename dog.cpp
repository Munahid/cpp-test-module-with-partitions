export module animals:dog;

import <string>;

export std::string dog() {
  return "Spike is a dog.";
}

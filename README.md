C++ module with two partitions.

$ make
rm -rf gcm.cache *.o
g++ -std=c++17 -fmodules-ts -x c++-system-header iostream
g++ -std=c++17 -fmodules-ts -x c++-system-header string
g++ -std=c++17 -fmodules-ts -c -x c++ cat.cpp -o cat.o
g++ -std=c++17 -fmodules-ts -c -x c++ dog.cpp -o dog.o
g++ -std=c++17 -fmodules-ts -c -x c++ animals.cpp -o animals.o
g++ -std=c++17 -fmodules-ts main.cpp -o main.exe animals.o cat.o dog.o
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: dog.o:dog.cpp:(.text+0x0): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::~_Alloc_hider()'; cat.o:cat.cpp:(.text+0x0): first defined here
C:/msys64/mingw64/bin/../lib/gcc/x86_64-w64-mingw32/11.2.0/../../../../x86_64-w64-mingw32/bin/ld.exe: dog.o:dog.cpp:(.text+0x0): multiple definition of `std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >::_Alloc_hider::~_Alloc_hider()'; cat.o:cat.cpp:(.text+0x0): first defined here
collect2.exe: error: ld returned 1 exit status
make: *** [Makefile:10: all] Error 1


Returning an int instead of a string works.
